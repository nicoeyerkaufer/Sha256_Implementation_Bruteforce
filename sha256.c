#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "sha256.h"

extern uint32_t hash[MAX_PASS_LEN+1];

static inline uint32_t right_rot(uint32_t x, unsigned int y)
{
    /*
     * Defined behaiour in standard C for all count where 0 < count < 32,
     */
   // return value >> count | value << (32 - count);

    return (((x & 0xffffffff) >> (y & 31)) | (x << (32 - (y & 31)))) & 0xffffffff;

}

int
sha256_process(uint8_t data[64], uint32_t hash[8])
{
    uint32_t A, B, C, D, E, F, G, H, a, b, c, d, e, f, g;
   uint32_t w[64];

    uint32_t h[] = { 0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19 };
    uint32_t ah[8];
    /* implement SHA rounds */

    static const uint32_t k[64] =
  {
    0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
    0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
    0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
    0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
    0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
    0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
    0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
    0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
    0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
    0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
    0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
    0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
    0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
    0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
    0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
    0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
  };

 for (int i = 0; i < 8; i++)
            ah[i] = h[i];

    // split up 512 bit chunk into 16 32 bit words
    // for i from 0 to 15
    // W[i] = chunk[i]
    memset(w, 0x00, sizeof w);
    const uint8_t *j = data;
    int p = 0;
    for (int i = 0; i < 16; ++i){
       // w[i] = data [i];
        w[i] = (uint32_t) j[0] << 24 | (uint32_t) j[1] << 16 |(uint32_t) j[2] << 8 | (uint32_t) j[3];
       //w[i] = j[0] | (uint32_t) j[1] << 8;
         j += 4;
    }
    //w[1] = 1;

  /*  for (int i = 0; i< 64;i++){
    printf("W bevor expansion %i: 0x%x \n",i,w[i]);
    }*/
    /*
    input expansion
    for i from 16 to 63
    tmp0 = (w[i-15] rotr 7) xor (w[i-15] rotr 18) xor (w[i-15] shiftr 3)
    tmp1 = (w[i-2] rotr 17) xor (w[i-2] rotr 19) xor (w[i-2] shiftr 10)
    w[i] = w[i-16] add tmp0 add w[i-7] add tmp1
    */
    for (int i = 16; i < 64; i++){
        const uint32_t tmp0 = right_rot(w[i - 15], 7) ^ right_rot(w[i - 15], 18) ^ (w[i - 15] >> 3);
        const uint32_t tmp1 = right_rot(w[i - 2], 17) ^ right_rot(w[i - 2], 19) ^ (w[i - 2] >> 10);
        w[i] = w[i-16] + tmp0 + w[i-7] + tmp1;
    }
 /*   for (int i = 0; i< 64;i++){
    printf("W nach expansion %i: 0x%x \n",i,w[i]);
    }*/

     /* Initialize working variables to current hash value: */

       // printf("ah0 \t\t ah1 \t\t ah2 \t\t ah3 \t\t ah4 \t\t ah5\t\t ah6 \t\t ah7 \n");
    // hash rounds per chunk
    //uint32_t tmp1, ch, tmp, tmp0, maj;
    for (int i = 0; i < 64; i++) {
            const uint32_t s1 = right_rot(ah[4], 6) ^ right_rot(ah[4], 11) ^ right_rot(ah[4], 25);
			const uint32_t ch = (ah[4] & ah[5]) ^ (~ah[4] & ah[6]);
			const uint32_t temp1 = ah[7] + s1 + ch + k[i] + w[i];
			const uint32_t s0 = right_rot(ah[0], 2) ^ right_rot(ah[0], 13) ^ right_rot(ah[0], 22);
			const uint32_t maj = (ah[0] & ah[1]) ^ (ah[0] & ah[2]) ^ (ah[1] & ah[2]);
			const uint32_t temp2 = s0 + maj;right_rot(ah[0], 2) ^ right_rot(ah[0], 13) ^ right_rot(ah[0], 22);

	/*		tmp1 = right_rot(ah[4],7) ^ right_rot(ah[4], 11) ^ right_rot(ah[4],25);
			ch = (ah[4] & ah[5]) ^ (~ah[4] & ah[6]);
			tmp = ah[7] + tmp1 + ch + k[i] + w[i];
			d = d + tmp;
			tmp0 = right_rot(ah[0], 2) ^ right_rot(ah[0], 13) ^ right_rot(ah[0], 22);
			maj = (ah[0] & (ah[1] ^ ah[2])) ^ (ah[1] & ah[2]);
			tmp = tmp + tmp0 + maj;
*/
			ah[7] = ah[6];
			ah[6] = ah[5];
			ah[5] = ah[4];
			ah[4] = ah[3] + temp1;
			ah[3] = ah[2];
			ah[2] = ah[1];
			ah[1] = ah[0];
            ah[0] = temp1 + temp2;

           /* for (int r = 7; r > 0; r--){
                ah[r] = ah[r-1];
            }
            ah[0] = tmp; */
           // printf("0x%x \t 0x%x \t 0x%x \t 0x%x \t 0x%x \t 0x%x \t 0x%x \t 0x%x \n",ah[0],ah[1],ah[2],ah[3],ah[4],ah[5],ah[6],ah[7]);
            //printf("%u \t %u \t %u \t %u \t %u \t %u \t %u \t %u \n",ah[0]+h[0],ah[1]+h[1],ah[2]+h[2],ah[3]+h[3],ah[4]+h[4],ah[5]+h[5],ah[6]+h[6],ah[7]+h[7]);
     }

   for (int i = 0; i < 8; i++)
    h[i] += ah[i];

  /*  char Same[] = "Ungleich";

    if (h[0] == hash[0]){
       char Same[] = "Gleich";
    }

    printf("\n\t %s \n\n",Same);


    for (int i = 0; i < 8;i++) {

        printf(" h[%i]: %u \t Hash[%i]: %u \n Hex \n h[%i]: 0x%x \t Hash[%i]: 0x%x \n\n\n",i,h[i],i,hash[i],i, h[i],i,hash[i]);

    }*/

    if (h[0] == hash[0] && h[1] == hash[1] && h[2] == hash[2] && h[3] == hash[3] &&
            h[4] == hash[4] && h[5] == hash[5] && h[6] == hash[6] && h[7] == hash[7])
        return 1;

  //  scanf("Stop here");
    return 0;
}

void
sha256_preprocess(unsigned char *input)
{

    /* Determine the length of the input */
    int len_bytes = strlen(input);
    uint8_t len_bits = len_bytes*8;

    /* Terminate input with '1' bit and pad message with zeroe to 448 bits */
    input[len_bytes] = 0x80;
    for (int i = len_bytes+1; i < 64; ++i)
        input[i] = 0x00;

     /*   for (int i = 0; i < 64; i++){
        printf("Input %i: %i\n", i, input[i]);
    }*/


    /* Set the last 64 bits to the message length (big endian). */
    input[63] = len_bits;

   /* for (int i = 0; i < 64; i++){
        printf("Input %i: %i\n", i, input[i]);
    }*/


    /* Change endianness from little (x32/ia64) to big */
 /*  for (int i = 0; i < 16; i++) {
            char tmp = input[i*4];
            input[i*4] = input[i*4+3];
            input[i*4+3] = tmp;
            tmp = input[i*4+1];
            input[i*4+1] = input[i*4+2];
            input[i*4+2] = tmp;
    }
     for (int i = 0; i < 64; i++){
        printf("Input %i: %i\n", i, input[i]);
    }*/

}
