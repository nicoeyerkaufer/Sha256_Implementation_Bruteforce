#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <omp.h>

#include "sha256.h"

unsigned long hash[MAX_PASS_LEN+1];

void
brute_force()
{
    //password in ASCII 112 97 115 115 119 111 114 100
/*
    uint8_t string[8];
    string [0] = 112;
    string [1] = 97;
    string [2] = 115;
    string [3] = 115;
    string [4] = 119;
    string [5] = 111;
    string [6] = 114;
    string [7] = 100;
    string [8] = '\0';

     uint8_t input[64];
     strncpy(input, string, 64);

      for (int i = 0; i < 64; i++){
        printf("Input %i: 0x%x\n", i, input[i]);
        }

        sha256_preprocess(input);
        if (sha256_process(input, hash)) {
            printf("Found: %s\n", string);
            exit(EXIT_SUCCESS);
        }

        printf("\nFinish found nothing");*/

    /* try all one-character passwords */

    printf("All one-character passwords\n");

    #pragma omp parallel for
    for (int ch0=32; ch0<128; ++ch0) {
        uint8_t string[1];
        string[1] = '\0';
        string[0] = ch0;

        uint8_t input[64];
        strncpy(input, string, 64);

     /*   for (int i = 0; i < 64; i++){
        printf("Input %i: %i\n", i, input[i]);
        }*/

        sha256_preprocess(input);
        if (sha256_process(input, hash)) {
            printf("Found: %s\n", string);
            exit(EXIT_SUCCESS);
        }
    }
     printf("All two-character passwords\n");
    /* try all two-character passwords */
    #pragma omp parallel for
    for (int ch1=32; ch1<128; ++ch1) {
        uint8_t string[2];
        string[2] = '\0';
        string[0] = ch1;
        #pragma omp parallel for
        for (int ch0=32; ch0<128; ++ch0) {
            string[1] = ch0;

            uint8_t input[64];
            strncpy(input, string, 64);
            sha256_preprocess(input);
            if (sha256_process(input, hash)) {
                printf("Found: %s\n", string);
                exit(EXIT_SUCCESS);
            }
        }
    }
     printf("All three-character passwords\n");
    /* try all three-character passwords */
    #pragma omp parallel for
    for (int ch2=32; ch2<128; ++ch2) {
        #pragma omp parallel for
        for (int ch1=32; ch1<128; ++ch1) {
            uint8_t string[3];
            string[3] = '\0';
            string[0] = ch2;
            string[1] = ch1;
            #pragma omp parallel for
            for (int ch0=32; ch0<128; ++ch0) {
                string[2] = ch0;

                uint8_t input[64];
                strncpy(input, string, 64);
                sha256_preprocess(input);
                if (sha256_process(input, hash)) {
                    printf("Found: %s\n", string);
                    exit(EXIT_SUCCESS);
                }
            }
        }
    }
     printf("All four-character passwords\n");
    /* try all four-character passwords */
    #pragma omp parallel for
    for (int ch3=32; ch3<128; ++ch3) {
        #pragma omp parallel for
        for (int ch2=32; ch2<128; ++ch2)
            #pragma omp parallel for
            for (int ch1=32; ch1<128; ++ch1) {
                uint8_t string[5];
                string[4] = '\0';
                string[0] = ch3;
                string[1] = ch2;
                string[2] = ch1;
                #pragma omp parallel for
                for (int ch0=32; ch0<128; ++ch0) {
                    string[3] = ch0;

                    uint8_t input[64];
                    strncpy(input, string, 64);
                    sha256_preprocess(input);
                    if (sha256_process(input, hash)) {
                        printf("Found: %s\n", string);
                        exit(EXIT_SUCCESS);
                    }
                }
            }
        }

     printf("All five-character passwords\n");
    /* try all five-character passwords */
    #pragma omp parallel for
    for (int ch4=32; ch4<128; ++ch4) {
        #pragma omp parallel for
        for (int ch3=32; ch3<128; ++ch3) {
            #pragma omp parallel for
            for (int ch2=32; ch2<128; ++ch2) {
                #pragma omp parallel for
                for (int ch1=32; ch1<128; ++ch1) {
                    uint8_t string[6];
                    string[5] = '\0';
                    string[0] = ch4;
                    string[1] = ch3;
                    string[2] = ch2;
                    string[3] = ch1;
                    #pragma omp parallel for
                    for (int ch0=32; ch0<128; ++ch0) {
                        string[4] = ch0;

                        uint8_t input[64];
                        strncpy(input, string, 64);
                        sha256_preprocess(input);
                        if (sha256_process(input, hash)) {
                            printf("Found: %s\n", string);
                            exit(EXIT_SUCCESS);
                        }
                    }
                }
            }
        }
    }
    printf("All six-character passwords\n");
    /* try all six-character passwords */
    #pragma omp parallel for
    for (int ch5=32; ch5<128; ++ch5) {
        uint8_t string[7];
        string[6] = '\0';
        string[0] = ch5;
        #pragma omp parallel for
        for (int ch4=32; ch4<128; ++ch4) {
            string[1] = ch4;
            #pragma omp parallel for
            for (int ch3=32; ch3<128; ++ch3) {
                string[2] = ch3;
                #pragma omp parallel for
                for (int ch2=32; ch2<128; ++ch2) {
                    string[3] = ch2;
                    #pragma omp parallel for
                    for (int ch1=32; ch1<128; ++ch1) {
                        string[4] = ch1;
                        #pragma omp parallel for
                        for (int ch0=32; ch0<128; ++ch0) {
                            string[5] = ch0;

                            uint8_t input[64];
                            strncpy(input, string, 64);
                            sha256_preprocess(input);
                            if (sha256_process(input, hash)) {
                                printf("Found: %s\n", string);
                                exit(EXIT_SUCCESS);
                            }
                        }
                    }
                }
            }
        }
    }

}

int
main(int argc, char *argv[])
{
    argc = 8;
    //argv[1] = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"; // empty
   // argv[1] = "CA978112CA1BBDCAFAC231B39A23DC4DA786EFF8147C4E72B9807785AFEE48BB"; //a
      argv[1] = "51C62D69A62C78D9A8335B92025008FB49C7AEA47A92271F07B54A6590820E25";
   // argv[1] = "5E884898DA28047151D0E56F8DC6292773603D0D6AABBDD62A11EF721D1542D8"; //password
    if (argc < 2) {
        printf("usage: %s <hash>\n", argv[0]);
        printf("\tWill try to brute-force a password that when hashed matches <hash>\n");
        printf("\te.g. %s 88d4266fd4e6338d13b845fcf289579d209c897823b9217da3e161936f031589\n", argv[0]);
        exit(1);
    }

    /* Convert input argv[1] (string) to uint32_t */
    for (int i = 0; i < HASH_SIZE; ++i) {
        char convert[CHUNK_SIZE+1];
        strncpy(convert, argv[1] + i*8, 8);
        convert[CHUNK_SIZE] = '\0';
        printf("%s\n",convert);
        hash[i] = (int)strtoul(convert, NULL, 16);
    }
   /* for (int i = 0; i < 8; i++){
    printf("Hash[%i]: %lu\t0x%x\n",i, hash[i], hash[i]);
    }*/
    printf("Searching for input to produce hash %s\n\n", argv[1]);

    brute_force();

    return 0;
}
