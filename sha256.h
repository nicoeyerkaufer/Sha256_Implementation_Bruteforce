#define MAX_PASS_LEN    8
#define HASH_SIZE       8
#define CHUNK_SIZE      8

int sha256_process(uint8_t *, uint32_t *);
void sha256_preprocess(unsigned char *);
void brute_force(void);
