.PHONY: all clean
CC=gcc
CFLAGS=-O3 -std=c99 -fopenmp

default: crack
all: default

crack: main.o sha256.o
	$(CC) $(CFLAGS) $(OFLAGS) $? -o $@

sha256.o: sha256.c
	$(CC) $(CFLAGS) $(OFLAGS) $< -c

main.o: main.c
	$(CC) $(CFLAGS) $(OFLAGS) $< -c

clean:
	rm -rf sha256.o main.o crack
